package ru.dzavrazhnov.dproject.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ViewController {

    @GetMapping("/main")
    public String getMainPage(Model model) {
        return "main_page";
    }

    @GetMapping("/record")
    public String getRecordPage(Model model) {
        return "record";
    }

    @GetMapping("/login")
    public String signInPage(Model model) {
        return "login";
    }

    @GetMapping("/sales")
    public String salesPage(Model model) {
        return "sales";
    }

    @GetMapping("/accounts")
    public String accountsPage(Model model) {
        return "accounts";
    }
}
