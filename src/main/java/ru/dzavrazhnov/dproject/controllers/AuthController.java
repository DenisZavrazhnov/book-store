package ru.dzavrazhnov.dproject.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.dzavrazhnov.dproject.dto.SignInDto;
import ru.dzavrazhnov.dproject.dto.SignUpDto;
import ru.dzavrazhnov.dproject.dto.TokenDTO;
import ru.dzavrazhnov.dproject.dto.UserDTO;
import ru.dzavrazhnov.dproject.service.SignInService;
import ru.dzavrazhnov.dproject.service.SignUpService;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequiredArgsConstructor
public class AuthController {
    private final SignUpService signUpService;
    private final SignInService signInService;

    @PostMapping("/signin")
    public TokenDTO getSignInPage(@RequestBody SignInDto sign){
        return signInService.signIn(sign);
    }

    @PostMapping("/signup")
    public UserDTO getSignUpPage(@RequestBody SignUpDto sign){
        return signUpService.signUp(sign);
    }
}
