package ru.dzavrazhnov.dproject.controllers;


import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.dzavrazhnov.dproject.dto.BookDto;
import ru.dzavrazhnov.dproject.dto.BooksStatisticsDto;
import ru.dzavrazhnov.dproject.dto.UserDTO;
import ru.dzavrazhnov.dproject.models.Book;
import ru.dzavrazhnov.dproject.service.BooksService;
import ru.dzavrazhnov.dproject.service.UsersService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@org.springframework.web.bind.annotation.RestController
@RequestMapping()
@RequiredArgsConstructor
public class RestController {

    private final BooksService booksService;
    private final UsersService usersService;

    @PostMapping("/saveBook")
    @PreAuthorize("hasAnyAuthority('USER')")
    public void saveBook(@RequestBody BookDto bookDTO) {
        booksService.saveBook(bookDTO);
    }

    @GetMapping("/getBooks")
    @PreAuthorize("hasAnyAuthority('USER')")
    public List<Book> getBooks() {
        return booksService.getAllBooks();
    }

    @PostMapping("/manyjson")
    @PreAuthorize("hasAnyAuthority('USER')")
    public void getMany(@RequestBody ArrayList<BookDto> bookDtos) {
        for (BookDto book : bookDtos) {
            booksService.saveBook(book);
        }
    }

    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasAnyAuthority('USER')")
    public void deleteBook(@PathVariable("id") Long id) {
        booksService.deleteById(id);
    }

    @GetMapping("/bookStats")
    @PreAuthorize("hasAnyAuthority('USER')")
    public BooksStatisticsDto getBookStats() {
        return booksService.bookStats();
    }

    @PostMapping("/sellBooks")
    @PreAuthorize("hasAnyAuthority('USER')")
    public void sellBooks(@RequestBody Integer[] list) {
        System.out.println(Arrays.toString(list));
        List<Integer> booksIds = Arrays.asList(list);
        booksService.sellBook(booksIds);
    }

    @GetMapping("/users")
    @PreAuthorize("hasAnyAuthority('USER')")
    public List<UserDTO> getUsers() {
        return usersService.getUsersAccounts();
    }
}
