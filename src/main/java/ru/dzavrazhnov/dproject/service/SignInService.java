package ru.dzavrazhnov.dproject.service;

import ru.dzavrazhnov.dproject.dto.SignInDto;
import ru.dzavrazhnov.dproject.dto.TokenDTO;
import ru.dzavrazhnov.dproject.dto.UserDTO;

public interface SignInService {
    TokenDTO signIn(SignInDto sign);
    boolean isValid(String token);

}
