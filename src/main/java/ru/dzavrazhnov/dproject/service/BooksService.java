package ru.dzavrazhnov.dproject.service;

import ru.dzavrazhnov.dproject.dto.BookDto;
import ru.dzavrazhnov.dproject.dto.BooksStatisticsDto;
import ru.dzavrazhnov.dproject.models.Book;

import java.util.List;

public interface BooksService {
    List<Book> getAllBooks();

    void saveBook(BookDto bookDTO);

    boolean bookContains(BookDto bookDTO);

    BooksStatisticsDto bookStats();

    void deleteById(long id);

    void sellBook(List<Integer> booksId);
}
