package ru.dzavrazhnov.dproject.service;

import ru.dzavrazhnov.dproject.models.Order;

public interface OrderService {
    void save(Order order);
}
