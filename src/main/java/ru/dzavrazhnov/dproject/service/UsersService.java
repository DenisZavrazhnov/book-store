package ru.dzavrazhnov.dproject.service;

import ru.dzavrazhnov.dproject.dto.UserDTO;
import ru.dzavrazhnov.dproject.models.User;

import java.util.List;

public interface UsersService {
    List<User> getAllUsers();

    void saveUser(User user);

    User getById(long id);

    List<UserDTO> getUsersAccounts();
}
