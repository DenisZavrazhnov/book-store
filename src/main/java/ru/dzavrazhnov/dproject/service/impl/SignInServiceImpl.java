package ru.dzavrazhnov.dproject.service.impl;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.dzavrazhnov.dproject.dto.SignInDto;
import ru.dzavrazhnov.dproject.dto.TokenDTO;
import ru.dzavrazhnov.dproject.dto.UserDTO;
import ru.dzavrazhnov.dproject.models.User;
import ru.dzavrazhnov.dproject.repositories.UserRepository;
import ru.dzavrazhnov.dproject.service.SignInService;

import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SignInServiceImpl implements SignInService {
    private final UserRepository usersRepository;
    private final PasswordEncoder passwordEncoder;

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expirationDateInMs}")
    private String jwtExpirationInMs;

    @Override
    public TokenDTO signIn(SignInDto sign) {
        // получаем пользователя по его id
        Optional<User> userOptional = usersRepository.findByLogin(sign.getLogin());
        // если у меня есть этот пользвователь
        if (userOptional.isPresent()) {
            // получаем его
            User user = userOptional.get();
            // если пароль подходит
            if (passwordEncoder.matches(sign.getPassword(), user.getPassword())) {
                // создаем токен
                String token = Jwts.builder()
                        .setSubject(String.valueOf(user.getUserId())) // id пользователя
                        .claim("name", user.getName()) // имя
                        .claim("role", user.getRole().name()) // роль
                        .signWith(SignatureAlgorithm.HS256, secret)// подписываем его с нашим secret
                        .setExpiration(new Date(System.currentTimeMillis() + Integer.parseInt(jwtExpirationInMs)))//устанавливаем срок действия
                        .compact(); // преобразовали в строку
                return new TokenDTO(token);
            } else throw new AccessDeniedException("Wrong password");
        } else throw new AccessDeniedException("User not found");
    }


    public boolean isValid(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();
        Date expirationDate = claims.getExpiration();
        Date localDate = new Date(System.currentTimeMillis());
        return expirationDate.after(localDate);
    }
}
