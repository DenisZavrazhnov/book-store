package ru.dzavrazhnov.dproject.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.dzavrazhnov.dproject.dto.UserDTO;
import ru.dzavrazhnov.dproject.models.Order;
import ru.dzavrazhnov.dproject.models.User;
import ru.dzavrazhnov.dproject.repositories.UserRepository;
import ru.dzavrazhnov.dproject.service.UsersService;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public void saveUser(User user) {
        userRepository.save(user);
    }

    @Override
    public User getById(long id) {
        return userRepository.getOne(id);
    }

    @Override
    public List<UserDTO> getUsersAccounts() {
        List<UserDTO> userList = new ArrayList<>();
        List<Order> orders = new ArrayList<>();
        List<User> users = userRepository.findAll();
        for (User u : users) {
            userList.add(UserDTO.fromEntity(u));
        }
        return userList;
    }

}
