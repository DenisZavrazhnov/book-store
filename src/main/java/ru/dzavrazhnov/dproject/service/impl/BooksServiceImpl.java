package ru.dzavrazhnov.dproject.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import ru.dzavrazhnov.dproject.dto.BookDto;
import ru.dzavrazhnov.dproject.dto.BooksStatisticsDto;
import ru.dzavrazhnov.dproject.models.Book;
import ru.dzavrazhnov.dproject.models.Order;
import ru.dzavrazhnov.dproject.repositories.BookRepository;
import ru.dzavrazhnov.dproject.repositories.OrderRepository;
import ru.dzavrazhnov.dproject.service.BooksService;
import ru.dzavrazhnov.dproject.service.UsersService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BooksServiceImpl implements BooksService {
    private final BookRepository bookRepository;
    private final OrderRepository orderRepository;
    private final UsersService usersService;

    @Override
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @Override
    public void saveBook(BookDto bookDTO) {
        if (bookContains(bookDTO)) {
            Book book = bookRepository.findBookByAuthorNameAndAndBookName(bookDTO.getAuthor(), bookDTO.getBookName());
            book.setCount(book.getCount() + bookDTO.getCount());
            bookRepository.save(book);
        } else {
            bookRepository.save(Book.fromDto(bookDTO));
        }
    }

    @Override
    public boolean bookContains(BookDto bookDTO) {
        List<Book> books = bookRepository.findAll();
        return books.stream()
                .filter(book1 -> book1.getBookName().equals(bookDTO.getBookName()))
                .anyMatch(book1 -> book1.getAuthorName().equals(bookDTO.getAuthor()));
    }

    @Override
    public BooksStatisticsDto bookStats() {
        List<Book> books = bookRepository.findAll();
        int price = 0;
        int unique = books.size();
        int total = 0;
        int bookSells = 0;
        int currentMoney = 0;
        for (Book book : books) {
            total = total + book.getCount();
            price = price + book.getBookPrice() * book.getCount();
        }
        List<Order> orders = orderRepository.findAll();
        for (Order o : orders) {
            bookSells = bookSells + o.getBooks().size();
            currentMoney = (int) (currentMoney + o.getTotalPrice());
        }
        return new BooksStatisticsDto(unique, total, price, bookSells, currentMoney);
    }

    @Override
    public void deleteById(long id) {
        bookRepository.deleteById(id);
    }

    @Override
    public void sellBook(List<Integer> booksId) {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        System.out.println(userDetails.getUsername());
        int price = 0;
        List<Book> bookList = new ArrayList<>();
        for (long id : booksId) {
            Optional<Book> book = bookRepository.findById(id);
            if (book.isPresent()) {
                if (book.get().getCount() - 1 < 0) {
                    throw new IllegalArgumentException("Нет в наличии");
                }
                price = price + book.get().getBookPrice();
                book.get().setCount(book.get().getCount() - 1);
                bookRepository.save(book.get());
                book.get().setCount(1);
                bookList.add(book.get());
                price = price + book.get().getBookPrice();
            }
        }
        Order bookOrder = Order.builder()
                .books(bookList)

                .totalPrice(price)
                .build();
        orderRepository.save(bookOrder);
    }

}
