package ru.dzavrazhnov.dproject.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.dzavrazhnov.dproject.dto.SignUpDto;
import ru.dzavrazhnov.dproject.dto.UserDTO;
import ru.dzavrazhnov.dproject.models.Role;
import ru.dzavrazhnov.dproject.models.State;
import ru.dzavrazhnov.dproject.models.User;
import ru.dzavrazhnov.dproject.service.SignUpService;
import ru.dzavrazhnov.dproject.service.UsersService;

import java.time.Instant;

@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {
    private final PasswordEncoder passwordEncoder;
    private final UsersService userService;

    @Override
    public UserDTO signUp(SignUpDto sign) {
        userService.getAllUsers().forEach(user -> {
            if (user.getLogin().equalsIgnoreCase(sign.getLogin())) {
                throw new IllegalArgumentException("User with login = " + sign.getLogin() + " already exist ");
            }
        });
        User user = User.builder()
                .login(sign.getLogin())
                .password(passwordEncoder.encode(sign.getPassword()))
                .name(sign.getName())
                .secondName(sign.getSecondName())
                .startDate(Instant.now())
                .role(Role.USER)
                .state(State.CONFIRMED)
                .build();
        userService.saveUser(user);
        return UserDTO.fromEntity(user);
    }
}
