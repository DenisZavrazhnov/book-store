package ru.dzavrazhnov.dproject.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.dzavrazhnov.dproject.models.Order;
import ru.dzavrazhnov.dproject.repositories.OrderRepository;
import ru.dzavrazhnov.dproject.service.OrderService;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;

    @Override
    public void save(Order order) {
        orderRepository.save(order);
    }
}
