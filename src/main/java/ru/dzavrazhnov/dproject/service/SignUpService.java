package ru.dzavrazhnov.dproject.service;

import ru.dzavrazhnov.dproject.dto.SignUpDto;
import ru.dzavrazhnov.dproject.dto.UserDTO;


public interface SignUpService {
    UserDTO signUp(SignUpDto sign);
}
