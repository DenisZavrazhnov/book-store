package ru.dzavrazhnov.dproject.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class BooksStatisticsDto {
    private int uniqueCounter;
    private int totalCounter;
    private int price;
    private int bookSells;
    private int currentMoney;
}
