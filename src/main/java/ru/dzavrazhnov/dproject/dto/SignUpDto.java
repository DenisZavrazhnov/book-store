package ru.dzavrazhnov.dproject.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "DTO to create new User, and getting JWT")
public class SignUpDto {

    @ApiModelProperty(notes = "Login")
    private String login;

    @ApiModelProperty(notes = "Password")
    private String password;

    @ApiModelProperty(notes = "Name")
    private String name;

    @ApiModelProperty(notes = "Lastname")
    private String secondName;
}
