package ru.dzavrazhnov.dproject.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Builder
@Data
@AllArgsConstructor
public class BookDto implements Serializable {
    @JsonProperty("id")
    private long id;
    @JsonProperty("bookName")
    private String bookName;
    @JsonProperty("author")
    private String author;
    @JsonProperty("bookPrice")
    private int bookPrice;
    @JsonProperty("count")
    private int count;
}
