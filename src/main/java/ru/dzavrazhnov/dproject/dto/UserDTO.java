package ru.dzavrazhnov.dproject.dto;

import lombok.Builder;
import lombok.Data;
import ru.dzavrazhnov.dproject.models.User;

import java.time.Instant;

@Builder
@Data
public class UserDTO {
    private long id;
    private String name;
    private String secondName;
    private String login;
    private String password;
    private int deals;
    private Instant startDate;

    public static UserDTO fromEntity(User user) {
        return UserDTO.builder()
                .id(user.getUserId())
                .name(user.getName())
                .secondName(user.getSecondName())
                .login(user.getLogin())
                .deals(user.getOrders().size())
                .startDate(user.getStartDate())
                .build();
    }
}
