package ru.dzavrazhnov.dproject.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@ApiModel(description = "DTO for JWT")
public class TokenDTO {

    @ApiModelProperty(notes = "JWT")
    private String token;
}
