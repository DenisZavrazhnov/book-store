package ru.dzavrazhnov.dproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.dzavrazhnov.dproject.models.Book;

@Repository
public interface BookRepository extends JpaRepository<Book,Long> {
    Book findBookByAuthorNameAndAndBookName(String authorName, String bookName);
}
