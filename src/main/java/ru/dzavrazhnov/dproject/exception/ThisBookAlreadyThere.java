package ru.dzavrazhnov.dproject.exception;

import ru.dzavrazhnov.dproject.dto.BookDto;

public class ThisBookAlreadyThere extends RuntimeException{
    public ThisBookAlreadyThere(BookDto bookDTO) {
        super("This book is already there " + bookDTO);
    }
}
