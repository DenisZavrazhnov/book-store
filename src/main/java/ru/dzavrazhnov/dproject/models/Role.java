package ru.dzavrazhnov.dproject.models;

public enum Role {
    USER, ADMIN, ACCOUNTANT
}
