package ru.dzavrazhnov.dproject.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.dzavrazhnov.dproject.dto.BookDto;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "BOOKS")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long Id;
    @Column(name = "BOOKNAME")
    private String bookName;
    @Column(name = "AUTHORNAME")
    private String authorName;
    @Column(name = "PRICE")
    private int bookPrice;
    @Column(name = "COUNT")
    private int count;

    public static Book fromDto(BookDto book) {
        return Book.builder()
                .bookName(book.getBookName())
                .authorName(book.getAuthor())
                .bookPrice(book.getBookPrice())
                .count(book.getCount()).build();
    }
}
