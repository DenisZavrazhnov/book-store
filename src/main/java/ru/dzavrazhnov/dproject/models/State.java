package ru.dzavrazhnov.dproject.models;

public enum State {
    CONFIRMED, NOT_CONFIRMED
}
