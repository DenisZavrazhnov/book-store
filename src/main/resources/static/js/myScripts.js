const form = document.getElementById("login_form")
form.addEventListener("submit", auth)
let total
function writeTable() {
    $.ajax({
        type: "GET",
        url: "/getBooks",
        dataType: "json",
        headers: {
            'Authorization': localStorage.getItem("token")
        },
        success(data) {
            getStats()
            $('#myTable tbody > tr').remove();
            for (let i = 0; i < data.length; i++) {
                const row = $('<tr>' +
                    '<td id="bId">' + data[i].id + '</td>' +
                    '<td>' + data[i].authorName + '</td>' +
                    '<td>' + data[i].bookName + '</td>' +
                    '<td>' + data[i].bookPrice + '</td>' +
                    '<td>' + data[i].count + '</td>' +
                    '<td>' + `<button type="button"  onclick="deleteBook(${data[i].id})">Удалить</button>` + '</td>' +
                    '</tr>');
                $('#tdata').append(row);
            }
        }
    })
}

function deleteBook(id) {
    $.ajax({
        type: "DELETE",
        url: "/delete/" + id,
        headers: {
            'Authorization': localStorage.getItem("token")
        },
        success() {
            writeTable()
        },
        statusCode: {
            500() {
                console.log("This book is already there");
            }
        },
        contentType: "application/json",

    })
}

function saveBook(title, author, price, count) {
    let alert = document.getElementById("alert");
    let newBook = {
        "bookName": title,
        "author": author,
        "bookPrice": price,
        "count": count,
    }
    $.ajax({
        type: "POST",
        url: "/saveBook",
        headers: {
            'Authorization': localStorage.getItem("token")
        },
        data: JSON.stringify(newBook),
        success() {
            alert.style.visibility = "visible"
            writeTable()
            clearInput()
        },
        statusCode: {
            500() {
                console.log("This book is already there");
            }
        },
        contentType:"application/json",

    })
}
function hideAlert(){
    let alert = document.getElementById("alert");
    alert.style.visibility = "hidden"
}

function clearInput() {
    document.getElementById("bookNameInput").value = ""
    document.getElementById("bookAuthorInput").value = ""
    document.getElementById("bookPriceInput").value = ""
    document.getElementById("bookCountInput").value = ""
}

function auth(event) {
    event.preventDefault()
    let auth = {
        "login": event.target.login.value,
        "password": event.target.password.value
    }
    $.ajax({
        type: "POST",
        url: "/signin",
        data: JSON.stringify(auth),
        success(token) {
            localStorage.setItem("token", token.token)
            afterAuth()
        },
        statusCode: {
            500() {
                console.log("This book is already there");
            }
        },
        contentType:"application/json",

    })
}

function afterAuth() {
    window.open("http://localhost:8080/main")
    // $.ajax({
    //     headers: {
    //         'Authorization': localStorage.getItem("token")
    //     },
    //     type: "GET",
    //     url: "/",
    //     success(data) {
    //         const newDoc = document.open("text/html", "replace");
    //         newDoc.write(data);
    //         newDoc.close();
    //         window.history.pushState({"html": data, "pageTitle": "gonvn"}, "ИС 'Книжный Магазин'", "/")
    //     }
    // })
}

function loadFromFile() {
    let files = document.getElementById("file-input").files;
    for (let i = 0; i < files.length; i++) {
        const file = files.item(i);
        file.text()
            .then(text => {
                const parsedFile = JSON.parse(text)
                $.ajax({
                    type: "POST",
                    url: "/manyjson",
                    data: JSON.stringify(parsedFile),
                    headers: {
                        'Authorization': localStorage.getItem("token")
                    },
                    success() {
                        writeTable()
                    },
                    statusCode: {
                        500() {
                            console.log("This book is already there");
                        }
                    },
                    contentType: "application/json",

                });
            });
    }
}

function getStats() {
    $.ajax({
        type: "GET",
        url: "/bookStats",
        headers: {
            'Authorization': localStorage.getItem("token")
        },
        success(response) {
            console.log(response)
            document.getElementById("uniqueCount").value = response.uniqueCounter
            document.getElementById("totalCount").value = response.totalCounter
            document.getElementById("totalPrice").value = response.price
            document.getElementById("totalBookSells").value = response.bookSells
        },
        statusCode: {
            500() {
                console.log("proizoshlo nekotoroe govno");
            }
        },
        contentType: "application/json",

    })
}

//onclick="toBucket(${data[i].id}, '${data[i].authorName}','${data[i].bookName}', ${data[i].bookPrice})"
function writeTableForSales() {
    $.ajax({
        type: "GET",
        url: "/getBooks",
        dataType: "json",
        headers: {
            'Authorization': localStorage.getItem("token")
        },
        success(data) {
            const govnoP = govnoProsloika()
            $('#bucket tbody > tr').remove();
            for (let i = 0; i < data.length; i++) {
                const row = $('<tr>' +
                    '<td id="bId">' + data[i].id + '</td>' +
                    '<td>' + data[i].authorName + '</td>' +
                    '<td>' + data[i].bookName + '</td>' +
                    '<td>' + data[i].bookPrice + '</td>' +
                    '<td>' + data[i].count + '</td>' +
                    '<td>' + `<button type="button"  id="${data[i].id}">В корзину</button>` + '</td>' +
                    '</tr>');
                $('#bdata').append(row);
            }
            for (let i = 0; i < data.length; i++) {
                document.getElementById(`${data[i].id}`).addEventListener("click", function (event) {
                    govnoP(data[i].id, data[i].authorName, data[i].bookName, data[i].bookPrice)
                })
            }
        }
    })
}

function ttPrice() {
    var total = 0
    return function (price) {
        console.log(total)
        total = total + price
        const row = $('<tr>' +
            '<td id="bId">' + "" + '</td>' +
            '<td>' + "" + '</td>' +
            '<td>' + "" + '</td>' +
            '<td>' + total + '</td>' +
            '</tr>');
        $('#cbdata').append(row);
    }

}

function govnoProsloika() {
    let govnoIds = []
    let govnoCount = 0
    document.getElementById("sellBooks").addEventListener("click", function () {
        $.ajax({
            type: "POST",
            url: "/sellBooks",
            data: JSON.stringify(govnoIds),
            headers: {
                'Authorization': localStorage.getItem("token")
            },
            contentType: "application/json",
        })
        console.log(govnoIds)
    })
    return function (id, author, title, price) {
        govnoIds.push(id)
        govnoCount = govnoCount + price
        toBucket(id, author, title, price, govnoCount)
    }
}

function toBucket(id, author, title, price, govnoprice) {
    table = document.getElementById("cbucket")
    console.log(table.rows.length)
    const bucket = ttPrice()
    if (table.rows.length !== 1) {
        table.deleteRow(table.rows.length - 1)
    }
    const row = $('<tr>' +
        '<td id="bId">' + id + '</td>' +
        '<td>' + author + '</td>' +
        '<td>' + title + '</td>' +
        '<td>' + price + '</td>' +
        '</tr>');
    $('#cbdata').append(row);
    bucket(govnoprice)
}


function sell() {
    const array = [];
    $('table#cbucket tr').each(function () {
        var array_row = [];
        $(this).find('td').each(function () {
            array_row.push($(this).text());
        });
        array.push(array_row);

    });
    currentPrice(JSON.stringify(array))
}

function tableSearch() {
    var phrase = document.getElementById('search-text');
    var table = document.getElementById('bucket');
    var regPhrase = new RegExp(phrase.value, 'i');
    var flag = false;
    for (var i = 1; i < table.rows.length; i++) {
        flag = false;
        for (var j = table.rows[i].cells.length - 1; j >= 0; j--) {
            flag = regPhrase.test(table.rows[i].cells[j].innerHTML);
            if (flag) break;
        }
        if (flag) {
            table.rows[i].style.display = "";
        } else {
            table.rows[i].style.display = "none";
        }

    }
}

function writeUsersTable() {
    $.ajax({
        type: "GET",
        url: "/users",
        dataType: "json",
        headers: {
            'Authorization': localStorage.getItem("token")
        },
        success(data) {
            $('#usersTable tbody > tr').remove();
            for (let i = 0; i < data.length; i++) {
                const row = $('<tr>' +
                    '<td id="bId">' + data[i].id + '</td>' +
                    '<td>' + data[i].name + '</td>' +
                    '<td>' + data[i].secondName + '</td>' +
                    '<td>' + data[i].login + '</td>' +
                    '<td>' + data[i].startDate + '</td>' +
                    '<td>' + data[i].deals + '</td>' +
                    '</tr>');
                $('#udata').append(row);
            }
        }
    })
}